<?php get_header(); ?>

	<main role="main" aria-label="Content">
		<!-- section -->

		
		<section class="section-wrap">

			<div class="posts-wrapper">

				<?php get_template_part('loop'); ?>

				<?php get_template_part('pagination'); ?>
				
			</div>

		</section>
		<!-- /section -->
	</main>

<?php /*get_sidebar();*/ ?>

<?php get_footer(); ?>

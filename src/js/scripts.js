(function( root, $, undefined ) {
	"use strict";

	$(function () {
		// DOM ready, take it away

		

	});

	

} ( this, jQuery ));

$(document).ready(function() {
	var biggestHeight = 0;
	// Loop through elements children to find & set the biggest height
	$(".section-wrap *").each(function(){
		// If this elements height is bigger than the biggestHeight
		if ($(this).height() > biggestHeight ) {
		  // Set the biggestHeight to this Height
		  biggestHeight = $(this).height();
		}
	});

	// Set the container height
	$(".section-wrap").height(biggestHeight);



	$(".nav").on("click", ".mob-menu", function() {
	  if ($(".mob-menu").hasClass("mob-menu_active")) {
	    $(".mob-menu").removeClass("mob-menu_active");
	    $(".menu").removeClass("opennav").addClass("closenav");
	    $("body").removeClass("stop-scrolling").css({"overflow": "auto"});
	  } else {
	    $(".mob-menu").addClass("mob-menu_active");
	    $(".menu").removeClass("closenav").addClass("opennav");
	    $("body").addClass("stop-scrolling").css({"overflow": "hidden"});
	  }
	});

});